#How to use
- You need an API key from teamwork to use this application. You can get this from the teamwork site http://developer.teamwork.com/index.cfm/page/enabletheapiandgetyourkey

Once you have your api key, paste it in app/globals.js, replacing the value your-api-key with your api key



#Install instructions
##Install Node 
###Windows
http://blog.teamtreehouse.com/install-node-js-npm-windows
###Mac
http://blog.teamtreehouse.com/install-node-js-npm-mac

##Install Grunt
npm install grunt-cli --save-dev


##Install Bower
npm install -g bower

##Then in command line at the root of the project:
npm install

bower install

grunt watch

##Then 
open build/index.html to start using the app

